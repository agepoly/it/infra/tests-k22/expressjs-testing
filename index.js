const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World from Express! MY_SECRET is : ' + (process.env.MY_SECRET || "Not Found :/"))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
